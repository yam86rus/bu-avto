package dto;

import java.time.LocalDate;

public class GetPositionResponse {
    private String mark;
    private String model;
    private Integer mileage;
    private Double price;
    private LocalDate create_date;


    public GetPositionResponse() {
    }

    public GetPositionResponse(String mark, String model, Integer mileage, Double price, LocalDate create_date) {
        this.mark = mark;
        this.model = model;
        this.mileage = mileage;
        this.price = price;
        this.create_date = create_date;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public LocalDate getCreate_date() {
        return create_date;
    }

    public void setCreate_date(LocalDate create_date) {
        this.create_date = create_date;
    }
}

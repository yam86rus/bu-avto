package dto;

import lombok.Data;

@Data
public class GetResponseMarkDTO {
    private Long id;
    private String name;
    private String country;

}

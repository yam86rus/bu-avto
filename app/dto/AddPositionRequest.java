package dto;

import java.time.LocalDate;

public class AddPositionRequest {
    private int model;
    private int mark;
    private LocalDate create_date;
    private Integer mileage;
    private Double price;

    public AddPositionRequest() {
    }

    public AddPositionRequest(int model, int mark, LocalDate create_date, Integer mileage, Double price) {
        this.model = model;
        this.mark = mark;
        this.create_date = create_date;
        this.mileage = mileage;
        this.price = price;
    }

    public int getModel() {
        return model;
    }

    public void setModel(int model) {
        this.model = model;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public LocalDate getCreate_date() {
        return create_date;
    }

    public void setCreate_date(LocalDate create_date) {
        this.create_date = create_date;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}

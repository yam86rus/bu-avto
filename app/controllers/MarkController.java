package controllers;

import dto.GetResponseMarkDTO;
import entity.Mark;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import service.MarkService;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.CompletionStage;

@Singleton
public class MarkController extends Controller {
    private final FormFactory formFactory;
    private final MarkService markService;

    @Inject
    public MarkController(MarkService markService, FormFactory formFactory) {
        this.markService = markService;
        this.formFactory = formFactory;
    }

    public CompletionStage<Result> getAllMark() {
        return markService.findAll().thenApplyAsync(marks -> ok(Json.toJson(marks)));
    }

    public CompletionStage<Result> getMarkById(Long id) {
        return markService.findById(id).thenApplyAsync(u -> ok(Json.toJson(u)));
    }

    public Result addMark(Http.Request request) {
        Mark mark = formFactory.form(Mark.class).bindFromRequest(request).get();
        CompletionStage<GetResponseMarkDTO> getResponseMarkDTOCompletionStage = markService.addMark(mark);
        return ok("Запись добавлена");
    }

    public Result addMarkIndex() {
        return ok(views.html.addMark.render());
    }
}

package controllers;

import entity.Korus;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import service.KorusService;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class KorusController extends Controller {
    private final KorusService korusService;

    @Inject
    public KorusController(KorusService korusService) {
        this.korusService = korusService;
    }

    public Result addNew(Http.Request request) {
        Korus[] korus = Json.fromJson(request.body().asJson(), Korus[].class);
        korusService.addNew(korus);
        return ok("Запись добавлена");
    }
}

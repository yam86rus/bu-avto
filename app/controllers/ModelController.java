package controllers;

import entity.Model;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import service.ModelService;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

@Singleton
public class ModelController extends Controller {
    private final ModelService modelService;

    @Inject
    public ModelController(ModelService modelService) {
        this.modelService = modelService;
    }

    public Result getAllModels() {
        List<Model> models = modelService.findAll();
        return ok(Json.toJson(models));
    }

    public Result getModelById(Long id) {
        Model model = modelService.findById(id);
        return ok(Json.toJson(model));
    }
}

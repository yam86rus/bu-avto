package controllers;

import play.mvc.Controller;
import play.mvc.Result;

public class HelloController extends Controller {

    public Result index() {
        return ok(views.html.hello.render());
    }

    public Result showName(String name) {
        return ok("Hello " + name);
    }
}

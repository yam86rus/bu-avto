package service;

import entity.Korus;
import lombok.extern.slf4j.Slf4j;
import mapping.KorusMapper;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
@Slf4j
public class KorusService {
    private final KorusMapper korusMapper;

    @Inject
    public KorusService(KorusMapper korusMapper) {
        this.korusMapper = korusMapper;
    }

    public void addNew(Korus[] korus) {
        for (Korus korus1 : korus) {
            korusMapper.addNew(korus1);
        }
    }

}

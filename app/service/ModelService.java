package service;

import entity.Model;
import repositories.ModelRepository;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

@Singleton
public class ModelService {
    private final ModelRepository modelRepository;

    @Inject
    public ModelService(ModelRepository modelRepository) {
        this.modelRepository = modelRepository;
    }

    public List<Model> findAll(){
        List<Model> models = modelRepository.findAll();
        return models;
    }

    public Model findById(Long id){
        Model model = modelRepository.findById(id);
        System.out.println(model.getBeginCreate());
        System.out.println(model.getFinishCreate());
        return model;
    }
}

package service;

import dto.GetResponseMarkDTO;
import entity.Mark;
import lombok.extern.slf4j.Slf4j;
import repositories.MarkRepository;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.concurrent.CompletionStage;

@Singleton
@Slf4j
public class MarkService {

    private final MarkRepository markRepository;

    @Inject
    public MarkService(MarkRepository markRepository) {
        this.markRepository = markRepository;
    }

    public CompletionStage<List<Mark>>  findAll() {
        return markRepository.findAll();
    }

    public CompletionStage<Mark> findById(Long id) {
        log.debug(">> selectById() id: {}", id);
        return markRepository.findById(id);
    }

    public CompletionStage<GetResponseMarkDTO> addMark(Mark mark) {
//        Mark newMark = new Mark();
//        newMark.setName("\n\rКакая-то \r\r\n\n\n\n\n      марка       \r    в Российской \n \n  \n   \n    \r Федерации \n \n \n\n\n");
//        newMark.setCountry("Какая-то \r\n\n\n\r\r\rстрана   на этой планете");
        return markRepository.addMark(clean(mark));
    }

    public static Mark clean(Mark mark) {
        String name = mark.getName().replaceAll("\\s{2,}", " ").trim();
        String country = mark.getCountry().replaceAll("\\s{2,}", " ").trim();
        mark.setName(name);
        mark.setCountry(country);
        System.out.println(mark);
        return mark;
    }
}

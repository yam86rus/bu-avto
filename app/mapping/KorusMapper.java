package mapping;

import entity.Korus;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface KorusMapper {
    void addNew(Korus korus);
}

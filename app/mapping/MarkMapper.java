package mapping;

import dto.GetResponseMarkDTO;
import entity.Mark;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MarkMapper {

    List<Mark> findAll();

    Mark findById(@Param("id") Long id);

    GetResponseMarkDTO addMark(Mark mark);

}

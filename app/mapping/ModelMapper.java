package mapping;

import entity.Model;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ModelMapper {
    List<Model> findAll();
    Model findById(@Param("id") Long id);
}

package entity;

import lombok.Data;

import java.util.UUID;

@Data
public class Korus {
    private Long id;
    private UUID originalFileUuid;
    private UUID signFileUuid;
    private Integer dssKepUlId;
}

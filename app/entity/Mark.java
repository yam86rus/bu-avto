package entity;

import lombok.Data;

import java.time.LocalDateTime;
@Data
public class Mark {
    private Integer id;
    private LocalDateTime creation_date;
    private String name;
    private String country;

}


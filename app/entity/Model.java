package entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class Model {
    private Long id;
    private String name;
    private Date beginCreate;
    private Date finishCreate;

}

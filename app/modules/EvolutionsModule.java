package modules;

import play.api.*;
import play.api.db.evolutions.ApplicationEvolutions;
import play.api.db.evolutions.ApplicationEvolutionsProvider;
import play.api.db.evolutions.DefaultEvolutionsApi;
import play.api.db.evolutions.DefaultEvolutionsConfigParser;
import play.api.db.evolutions.EvolutionsApi;
import play.api.db.evolutions.EvolutionsConfig;
import play.api.db.evolutions.EvolutionsReader;
import play.api.inject.*;
import scala.collection.Seq;

public class EvolutionsModule extends Module {
    @Override
    public Seq<Binding<?>> bindings(Environment environment, Configuration configuration) {
        return seq(
                bind(EvolutionsConfig.class).toProvider(DefaultEvolutionsConfigParser.class)
                , bind(EvolutionsReader.class).to(RboEvolutionsReader.class)
                , bind(EvolutionsApi.class).to(DefaultEvolutionsApi.class)
                , bind(ApplicationEvolutions.class).toProvider(ApplicationEvolutionsProvider.class).eagerly()
        );
    }
}

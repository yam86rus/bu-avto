package modules;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Inject;
import javax.inject.Singleton;


import play.Environment;
import play.api.db.evolutions.Evolutions;
import play.api.db.evolutions.ResourceEvolutionsReader;
import scala.Option;
@Singleton
public class RboEvolutionsReader extends ResourceEvolutionsReader{
    private final Environment environment;

    private boolean fileNamesChecked;

    @Inject
    public RboEvolutionsReader(final Environment environment) {
        this.environment = environment;
    }

    @Override
    public Option<InputStream> loadResource(final String db, final int revision) {
        checkFileNamesFormat(db);

        final Pattern filePattern = Pattern.compile("^(\\d{2})_.*\\.sql$");


        final File[] fileNamesList = environment.getFile(Evolutions.directoryName(db))
                .listFiles(file -> filePattern.matcher(file.getName()).matches());

        if (fileNamesList != null
                && fileNamesList.length > 0
                && fileNamesList.length >= revision) {
            Arrays.sort(fileNamesList, Comparator.comparing(File::getName));
            try {
                final File file = fileNamesList[revision - 1];
                final String name = file.getName();
                final Matcher matcher = filePattern.matcher(name);

                if (!matcher.matches()
                        || revision != Integer.valueOf(matcher.group(1))) {
                    throw new RuntimeException("Illegal revision for filename: " + name + " Revision: " + revision);
                }

                return Option.apply(new FileInputStream(file));
            } catch (final FileNotFoundException e) {
                return Option.apply(null);
            }
        } else {
            return Option.apply(null);
        }
    }

    private void checkFileNamesFormat(final String db) {
        if (fileNamesChecked) {
            return;
        }

        final Pattern fileNamePattern = Pattern.compile("^\\d{2}_.*\\.sql$");
        final File[] files = environment.getFile(Evolutions.directoryName(db))
                .listFiles(file -> !fileNamePattern.matcher(file.getName()).matches());

        if (files != null
                && files.length > 0) {
            throw new RuntimeException("There are illegal evolution ddl-scripts names: " + Arrays.toString(files));
        }

        fileNamesChecked = true;
    }
}

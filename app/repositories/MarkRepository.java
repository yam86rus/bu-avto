package repositories;

import dto.GetResponseMarkDTO;
import entity.Mark;
import lombok.extern.slf4j.Slf4j;
import mapping.MarkMapper;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Singleton
@Slf4j
public class MarkRepository {
    private final MarkMapper markMapper;

    @Inject
    public MarkRepository(MarkMapper markMapper) {
        this.markMapper = markMapper;
    }

    public CompletionStage<List<Mark>> findAll() {
        log.debug(">> selectAll() ");
        return CompletableFuture.supplyAsync(markMapper::findAll);
    }

    public CompletionStage<Mark> findById(final long id) {
        log.debug(">> selectById() id: {}", id);
        return CompletableFuture.supplyAsync(() -> markMapper.findById(id));
    }

    public CompletionStage<GetResponseMarkDTO> addMark(final Mark mark) {
        log.debug(">> insert() ");
        return CompletableFuture.supplyAsync(() -> markMapper.addMark(mark));
    }
}

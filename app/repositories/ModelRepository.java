package repositories;

import entity.Model;
import mapping.ModelMapper;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

@Singleton
public class ModelRepository {

    private final ModelMapper modelMapper;

    @Inject
    public ModelRepository(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public List<Model> findAll() {
        List<Model> models = modelMapper.findAll();
        return models;
    }

    public Model findById(Long id){
        Model model = modelMapper.findById(id);
        return model;
    }
}

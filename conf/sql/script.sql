----------------------------------------------------------
-- Таблица "Марка"
----------------------------------------------------------

create table if not exists mark
(
    id      serial primary key not null, -- ключ записи
    name    varchar            null,     -- наименование
    country varchar            null      -- страна
);

comment on table mark is 'Таблица "Марка"';
comment on column mark.id is 'Ключ записи';
comment on column mark.name is 'Наименование';
comment on column mark.country is 'Страна';

insert into mark (name, country)
values ('Audi', 'Germany'),
       ('Bmw', 'Germany'),
       ('Ford', 'Usa'),
       ('Honda', 'Japan'),
       ('Suzuki', 'Japan'),
       ('Toyota', 'China');

----------------------------------------------------------
-- Таблица "Модель"
----------------------------------------------------------

create table if not exists model
(
    id            serial primary key not null, -- Ключ записи
    name          varchar            null,     -- Наименование
    begin_create  date               null,     -- Год начала производства
    finish_create date               null      -- Год окончания производства
);


comment on table model is 'Таблица "Модель"';
comment on column model.id is 'Ключ записи';
comment on column model.name is 'Наименование';
comment on column model.begin_create is 'Год начала производства';
comment on column model.finish_create is 'Год окончания производства';

insert into model (name, begin_create, finish_create)
values ('Audi A3 (8P) хэтчбек 5D', '2003-01-01', '2012-01-01'),
       ('Audi A3 (8V) хэтчбек 5D, седан', '2012-01-01', null),
       ('Audi A4 B5', '1995-01-01', '2001-01-01'),
       ('BMW 7 F01', '2008-01-01', null),
       ('BMW 5 GT F07', '2009-01-01', null),
       ('BMW 3 F30', '2012-01-01', null),
       ('FORD S-MAX', '2006-01-01', null),
       ('FORD Focus 2 coupe', '2005-01-01', '2010-01-01'),
       ('FORD Kuga 1', '2008-01-01', '2012-01-01'),
       ('Honda Accord 7', '2002-01-01', '2008-01-01'),
       ('Honda JAZZ', '2001-01-01', '2007-01-01'),
       ('Honda CR-V 1', '1995-01-01', '2002-01-01'),
       ('Suzuki Grand Vitara (5D)', '2005-01-01', null),
       ('Suzuki Jimny', '1998-01-01', null),
       ('Suzuki Liana', '2002-01-01', '2008-01-01'),
       ('Toyota Camry V20', '1996-01-01', '2002-01-01'),
       ('Toyota Corolla E150', '2006-01-01', '2013-01-01'),
       ('Toyota Prado LC150', '2009-01-01', null);

----------------------------------------------------------
-- Таблица "Позиция в магазине"
----------------------------------------------------------

create table if not exists position
(
    id          serial primary key not null, -- Ключ записи
    mark        int                null,     -- Внешний ключ на таблицу "Марка"
    model       int                null,     -- Внешний ключ на таблицу "Модель"
    create_date date               null,     -- Год выпуска
    mileage     int                null,     -- Пробег
    price       numeric            null,     -- Цена
    constraint fk_mark
        foreign key (mark)
            references mark (id),
    constraint fk_model
        foreign key (model)
            references model (id)
);

comment on table position is 'Таблица "Позиция в магазине"';
comment on column position.id is 'Ключ записи';
comment on column position.mark is 'Внешний ключ на таблицу "Марка"';
comment on column position.model is 'Внешний ключ на таблицу "Модель"';
comment on column position.create_date is 'Год выпуска';
comment on column position.mileage is 'Пробег';
comment on column position.price is 'Цена';

insert into position (mark, model, create_date, mileage, price)
VALUES (1, 1, '2004-01-01', 125015, 1250000),
       (1, 1, '2005-01-01', 85000, 1500000),
       (1, 2, '2013-01-01', 45000, 1300000),
       (1, 2, '2015-01-01', 25000, 1800000),
       (1, 3, '2001-01-01', 25000, 1800000),
       (1, 3, '1998-01-01', 115000, 1800000),

       (2, 4, '2009-01-01', 95100, 750000),
       (2, 4, '2011-01-01', 124000, 950000),
       (2, 5, '2010-01-01', 163000, 1800000),
       (2, 5, '2012-01-01', 147000, 1650000),
       (2, 6, '2013-01-01', 125000, 1300000),
       (2, 6, '2014-01-01', 110000, 1450000);
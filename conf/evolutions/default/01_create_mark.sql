-- !Ups

----------------------------------------------------------
-- Таблица "Марка"
----------------------------------------------------------

create table if not exists mark
(
    id            serial primary key not null, -- ключ записи
    creation_date timestamp(6)       not null, -- дата создания записи
    name          varchar            not null, -- наименование
    country       varchar            not null  -- страна
);

comment on table mark is 'Таблица "Марка"';
comment on column mark.id is 'Ключ записи';
comment on column mark.creation_date is 'Дата создания записи';
comment on column mark.name is 'Наименование';
comment on column mark.country is 'Страна';

-- !Downs
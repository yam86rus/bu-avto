-- !Ups

----------------------------------------------------------
-- Таблица "Позиция в магазине"
----------------------------------------------------------

create table if not exists korus
(
    id               serial primary key not null,
    originalFileUuid uuid               null,
    signFileUuid     uuid               null,
    dssKepUlId       integer            null
);


-- !Downs
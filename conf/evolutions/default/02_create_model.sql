-- !Ups

----------------------------------------------------------
-- Таблица "Модель"
----------------------------------------------------------

create table if not exists model
(
    id            serial primary key not null, -- Ключ записи
    name          varchar            null,     -- Наименование
    begin_create  date               null,     -- Год начала производства
    finish_create date               null      -- Год окончания производства
);


comment on table model is 'Таблица "Модель"';
comment on column model.id is 'Ключ записи';
comment on column model.name is 'Наименование';
comment on column model.begin_create is 'Год начала производства';
comment on column model.finish_create is 'Год окончания производства';

-- !Downs
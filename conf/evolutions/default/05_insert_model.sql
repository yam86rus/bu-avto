-- !Ups


insert into model (name, begin_create, finish_create)
values ('Audi A3 (8P) хэтчбек 5D', '2003-01-01', '2012-01-01'),
       ('Audi A3 (8V) хэтчбек 5D, седан', '2012-01-01', null),
       ('Audi A4 B5', '1995-01-01', '2001-01-01'),
       ('BMW 7 F01', '2008-01-01', null),
       ('BMW 5 GT F07', '2009-01-01', null),
       ('BMW 3 F30', '2012-01-01', null),
       ('FORD S-MAX', '2006-01-01', null),
       ('FORD Focus 2 coupe', '2005-01-01', '2010-01-01'),
       ('FORD Kuga 1', '2008-01-01', '2012-01-01'),
       ('Honda Accord 7', '2002-01-01', '2008-01-01'),
       ('Honda JAZZ', '2001-01-01', '2007-01-01'),
       ('Honda CR-V 1', '1995-01-01', '2002-01-01'),
       ('Suzuki Grand Vitara (5D)', '2005-01-01', null),
       ('Suzuki Jimny', '1998-01-01', null),
       ('Suzuki Liana', '2002-01-01', '2008-01-01'),
       ('Toyota Camry V20', '1996-01-01', '2002-01-01'),
       ('Toyota Corolla E150', '2006-01-01', '2013-01-01'),
       ('Toyota Prado LC150', '2009-01-01', null);

-- !Downs
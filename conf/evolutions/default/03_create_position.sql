-- !Ups

----------------------------------------------------------
-- Таблица "Позиция в магазине"
----------------------------------------------------------

create table if not exists position
(
    id          serial primary key not null, -- Ключ записи
    mark        int                null,     -- Внешний ключ на таблицу "Марка"
    model       int                null,     -- Внешний ключ на таблицу "Модель"
    create_date date               null,     -- Год выпуска
    mileage     int                null,     -- Пробег
    price       numeric            null,     -- Цена
    constraint fk_mark
        foreign key (mark)
            references mark (id),
    constraint fk_model
        foreign key (model)
            references model (id)
);

comment on table position is 'Таблица "Позиция в магазине"';
comment on column position.id is 'Ключ записи';
comment on column position.mark is 'Внешний ключ на таблицу "Марка"';
comment on column position.model is 'Внешний ключ на таблицу "Модель"';
comment on column position.create_date is 'Год выпуска';
comment on column position.mileage is 'Пробег';
comment on column position.price is 'Цена';

-- !Downs
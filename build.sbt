name := "buAvto"
organization := "ru.buAvto"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.10"

libraryDependencies += guice
libraryDependencies += jdbc
libraryDependencies += javaJdbc
libraryDependencies += evolutions

// https://mvnrepository.com/artifact/org.mybatis/mybatis
libraryDependencies += "org.mybatis" % "mybatis" % "3.5.11"

// https://mvnrepository.com/artifact/org.mybatis/mybatis-guice
libraryDependencies += "org.mybatis" % "mybatis-guice" % "3.17"

// https://mvnrepository.com/artifact/com.google.inject.extensions/guice-multibindings
libraryDependencies += "com.google.inject.extensions" % "guice-multibindings" % "4.2.3"

// https://mvnrepository.com/artifact/com.typesafe.play/play-jdbc
libraryDependencies += "com.typesafe.play" %% "play-jdbc" % "2.8.18"

// https://mvnrepository.com/artifact/org.postgresql/postgresql
libraryDependencies += "org.postgresql" % "postgresql" % "42.5.0"

// https://mvnrepository.com/artifact/org.projectlombok/lombok
libraryDependencies += "org.projectlombok" % "lombok" % "1.18.26" % "provided"

// https://mvnrepository.com/artifact/org.projectlombok/lombok-mapstruct-binding
libraryDependencies += "org.projectlombok" % "lombok-mapstruct-binding" % "0.2.0"


routesGenerator := InjectedRoutesGenerator
